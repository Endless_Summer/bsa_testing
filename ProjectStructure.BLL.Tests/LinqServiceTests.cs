﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
    public class LinqServiceTests : IClassFixture<ServiceFixture>, IDisposable
    {
        private readonly ILinqService service;
        private readonly ProjectStructureDbContext context;
        private readonly IUnitOfWork unitOfWork;

        public LinqServiceTests(ServiceFixture fixture)
        {
            DbContextOptions<ProjectStructureDbContext> options = new DbContextOptionsBuilder<ProjectStructureDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDB") // , new InMemoryDatabaseRoot()
                .Options;

            context = new ProjectStructureDbContext(options);

            context.Database.EnsureCreated();

            unitOfWork = new UnitOfWork(context);

            service = new LinqService(unitOfWork, fixture.GetMapper);
        }
        public void Dispose()
        {
            context.Database.EnsureDeleted();
        }
        // Task 1
        [Theory]
        [InlineData(1, 2, 24, 1)]
        [InlineData(2, 0, 0, 0)]
        [InlineData(3, 2, 3, 3)]
        [InlineData(4, 2, 16, 3)]
        [InlineData(5, 0, 0, 0)]
        public void GetProjectUserTasksCount_WhenCondition_ThenGetData(int userId, int expectedCount, int expectedActualIdProjectIndex0, int expectedUserTasksCountIndex0)
        {
            List<ProjectUserTasksCountDTO> result = service.GetProjectUserTasksCount(userId).ToList();

            int actualCount = result.Count();

            ProjectUserTasksCountDTO prj = result.FirstOrDefault(p => p.Project.Id == expectedActualIdProjectIndex0);
            int actualIdProjectIndex0 = prj == null ? 0 : prj.Project.Id;
            int actualExpectedUserTasksCountIndex0 = prj == null ? 0 : prj.UserTasksCount;

            // Assert
            Assert.Equal(expectedCount, actualCount);
            Assert.Equal(expectedActualIdProjectIndex0, actualIdProjectIndex0);
            Assert.Equal(expectedUserTasksCountIndex0, actualExpectedUserTasksCountIndex0);
        }

        // Task 2
        [Theory]
        [InlineData(1, 2, 32, "Necessitatibus beatae animi unde velit nisi.")]
        [InlineData(2, 1, 4, "Repellendus itaque expedita est ut.")]
        [InlineData(3, 2, 167, "Fuga adipisci quo iusto recusandae enim.")]
        [InlineData(4, 1, 173, "Exercitationem modi reprehenderit debitis.")]
        [InlineData(5, 1, 155, "Omnis nostrum occaecati consectetur sint.")]
        public void GetUserTasks_WhenCondition_ThenGetData(int taskId, int expectedCount, int expectedActualIdTaskndex0, string expectedTaskNameIndex0)
        {
            List<TaskDTO> result = service.GetUserTasks(taskId).ToList();

            int actualCount = result.Count();
            TaskDTO task = result.FirstOrDefault(t => t.Id == expectedActualIdTaskndex0);


            int actualIdTaskIndex0 = task == null ? 0 : task.Id;
            string actualExpectedTaskNameIndex0 = task == null ? null : task.Name;

            // Assert
            Assert.Equal(expectedCount, actualCount);
            Assert.Equal(expectedActualIdTaskndex0, actualIdTaskIndex0);
            Assert.Equal(expectedTaskNameIndex0, actualExpectedTaskNameIndex0);
        }

        // Task 3
        [Theory]
        [InlineData(1, 1, 32, "Necessitatibus beatae animi unde velit nisi.")]
        [InlineData(2, 1, 4, "Repellendus itaque expedita est ut.")]
        [InlineData(3, 0, 0, null)]
        [InlineData(4, 0, 0, null)]
        [InlineData(5, 0, 0, null)]
        public void GetFinishedTasksForUser_WhenCondition_ThenGetData(int userId, int expectedCount, int expectedActualIdTaskndex0, string expectedTaskNameIndex0)
        {
            List<TaskShortDTO> result = service.GetFinishedTasksForUser(userId).ToList();

            int actualCount = result.Count();
            TaskShortDTO task = result.FirstOrDefault(t => t.Id == expectedActualIdTaskndex0);


            int actualIdTaskIndex0 = task == null ? 0 : task.Id;
            string actualExpectedTaskNameIndex0 = task == null ? null : task.Name;

            // Assert
            Assert.Equal(expectedCount, actualCount);
            Assert.Equal(expectedActualIdTaskndex0, actualIdTaskIndex0);
            Assert.Equal(expectedTaskNameIndex0, actualExpectedTaskNameIndex0);
        }

        // Task 4
        [Fact]
        public void GetAgeLimitTeams_WhenCondition_ThenGetData()
        {
            int projectId = 4;
            int expectedCount = 6;
            int[] expectedMember = new int[] { 1, 12, 10, 42 };
            List<TeamUsersDTO> result = service.GetAgeLimitTeams().ToList();

            int actualCount = result.Count();

            TeamUsersDTO task = result.FirstOrDefault(t => t.Id == projectId);

            var actualMember = task.Users.Select(u => u.Id).ToArray();

            Array.Sort(actualMember);
            Array.Sort(expectedMember);

            // Assert
            Assert.Equal(expectedCount, actualCount);
            Assert.True(actualMember.SequenceEqual(expectedMember));

        }


        // Task 5
        [Fact]
        public void GetSortedUsers_WhenCondition_ThenGetData()
        {
            int[] expectedTasksForUserId23 = new int[] { 197, 184, 53 };
            List<UserTasksDTO> result = service.GetSortedUsers().ToList();

            int actualCount = result.Count();

            UserTasksDTO user = result.FirstOrDefault(t => t.User.Id == 23);
            UserTasksDTO userFirst = result.FirstOrDefault();
            UserTasksDTO userLast = result.LastOrDefault();

            var actualTasksForUserId23 = user.Tasks.Select(t => t.Id).ToArray();

            Array.Sort(expectedTasksForUserId23);
            Array.Sort(actualTasksForUserId23);

            //// Assert
            Assert.Equal(50, actualCount);
            Assert.True(actualTasksForUserId23.SequenceEqual(expectedTasksForUserId23));
            Assert.Equal("Abe", userFirst.User.FirstName);
            Assert.Equal("Zane", userLast.User.FirstName);
        }

        // Task 6
        [Theory]
        [InlineData(1, "Jordane", 36, 0, 2, 117)]
        [InlineData(2, "Kolby", 0, 0, 4, 181)]
        [InlineData(3, "Mabelle", 3, 3, 8, 64)]
        [InlineData(4, "Verla", 16, 3, 4, 173)]
        [InlineData(5, "Retha", 0, 0, 1, 155)]
        public void GetUserLastProjectInfo_WhenCondition_ThenGetData(int userId, string expectedName, int expectedProjectId, int expectedTasksLastProject,
            int expectedNotComletedTasks, int expectedMaxTaskId)
        {
            UserInfoDTO result = service.GetUserLastProjectInfo(userId);

            int actualProjectId = result.LastProject == null ? 0 : result.LastProject.Id;

            // Assert
            Assert.Equal(expectedName, result.User.FirstName);
            Assert.Equal(expectedProjectId, actualProjectId);
            Assert.Equal(expectedTasksLastProject, result.TasksLastProject);
            Assert.Equal(expectedNotComletedTasks, result.NotComletedTasks);
            Assert.Equal(expectedMaxTaskId, result.MaxTask.Id);
        }


        // Task 7
        [Fact]
        public void GetProjectShortInfo_WhenCondition_ThenGetData()
        {
            List<ProjectShortInfo> result = service.GetProjectShortInfo().ToList();

            int actualCount = result.Count();

            ProjectShortInfo userFirst = result.FirstOrDefault();
            ProjectShortInfo userLast = result.LastOrDefault();

            // Assert
            Assert.Equal(100, actualCount);
            Assert.Equal("Expedita amet quas id a.", userFirst.Project.Name);
            Assert.Equal(135, userFirst.LongDescriptionTask.Id);
            Assert.Equal(135, userFirst.ShortestNameTask.Id);
            Assert.Equal(0, userFirst.UserCount);

            Assert.Equal("Quam veniam distinctio ut magnam.", userLast.Project.Name);
            Assert.Equal(182, userLast.LongDescriptionTask.Id);
            Assert.Equal(182, userLast.ShortestNameTask.Id);
            Assert.Equal(0, userLast.UserCount);
        }

    }
}
