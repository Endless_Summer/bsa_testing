﻿namespace ProjectStructure.Common.Models
{
    public class ProjectUserTasksCountDTO
    {
        public ProjectDTO Project { get; set; }
        public int UserTasksCount { get; set; }
    }
}
