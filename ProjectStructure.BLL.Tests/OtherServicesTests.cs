﻿using AutoMapper;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
    public class OtherServicesTests : IClassFixture<ServiceFixture>, IDisposable
    {
        private readonly IUsersService _usersService;
        private readonly ITasksService _tasksService;
        private readonly ProjectStructureDbContext context;
        private readonly IUnitOfWork unitOfWork;

        readonly IUnitOfWork _fakeUnitOfWork;
        readonly IMapper _imapper;

        public OtherServicesTests(ServiceFixture fixture)
        {
            DbContextOptions<ProjectStructureDbContext> options = new DbContextOptionsBuilder<ProjectStructureDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDB", new InMemoryDatabaseRoot())
                .Options;

            context = new ProjectStructureDbContext(options);

            context.Database.EnsureCreated();

            unitOfWork = new UnitOfWork(context);

            _usersService = new UsersService(unitOfWork, fixture.GetMapper);
            _tasksService = new TasksService(unitOfWork, fixture.GetMapper);


            _fakeUnitOfWork = A.Fake<IUnitOfWork>();
            _imapper = A.Fake<IMapper>();

        }
        public void Dispose()
        {
            context.Database.EnsureDeleted();
        }

        [Fact]
        public void CreateUser_WhenRightDTO_ThenAddUserToDB()
        {
            var countUsersBeforeCreate = _usersService.GetUsers().Count();

            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 2,
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            _usersService.CreateUser(userDTO);

            var countUsersAfterCreate = _usersService.GetUsers().Count();
            var userTest = _usersService.GetUsers().FirstOrDefault(u => u.FirstName == "FirstName" && u.LastName == "LastName");

            Assert.True(countUsersAfterCreate - countUsersBeforeCreate == 1);

            Assert.NotNull(userTest);
            Assert.Equal("FirstName", userTest.FirstName);
            Assert.Equal("LastName", userTest.LastName);
            Assert.Equal("email@mail.com", userTest.Email);
            Assert.Equal(2, userTest.TeamId);
        }

        [Fact]
        public void CreateUser_WhenCallServiceMethod_ThenCallDalMethod()
        {
            UsersService usersService = new UsersService(_fakeUnitOfWork, _imapper);

            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 2,
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            usersService.CreateUser(userDTO);

            //A.CallTo(() => _fakeUnitOfWork.Users.Create(A<User>.That.IsInstanceOf(typeof(User)))).MustHaveHappenedOnceExactly();
            
            A.CallTo(() => _fakeUnitOfWork.Users.Create(A<User>._)).MustHaveHappenedOnceExactly();
        }



        [Fact]
        public void CreateUser_WhenFalseTeamIdDTO_ThenThrowArgumentException()
        {
            var countUsersBeforeCreate = _usersService.GetUsers().Count();

            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 99999,
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            var countUsersAfterCreate = _usersService.GetUsers().Count();

            Assert.Throws<ArgumentException>(() => _usersService.CreateUser(userDTO));
            Assert.True(countUsersAfterCreate - countUsersBeforeCreate == 0);
        }

        [Fact]
        public void ChangeTaskToComplete_WhenTrueDTO_ThenChanged()
        {
            var taskStateCreated = _tasksService.GetTasks(true).FirstOrDefault(t => t.State == 1);
            int idTaskState = taskStateCreated.Id;

            taskStateCreated.State = 2;
            var newTask = new TaskDTO()
            {
                Id = taskStateCreated.Id,
                Name = taskStateCreated.Name,
                Description = taskStateCreated.Description,
                CreatedAt = taskStateCreated.CreatedAt,
                FinishedAt = taskStateCreated.FinishedAt,
                PerformerId = taskStateCreated.PerformerId,
                ProjectId = taskStateCreated.ProjectId,
                State = taskStateCreated.State
            };

            _tasksService.UpdateTask(newTask);

            var taskStateChanged = _tasksService.FindTaskById(idTaskState);

            Assert.Equal(2, taskStateChanged.State);
        }

        [Fact]
        public void ChangeTaskToComplete_WhenCallServiceMethod_ThenCallDalMethod()
        {
            TasksService tasksService = new TasksService(_fakeUnitOfWork, _imapper);

            var task = new TaskDTO()
            {
                Id = 2,
                Name = "Name",
                Description = "Description",
                CreatedAt = DateTime.Now,
                FinishedAt = DateTime.Now,
                PerformerId = 1,
                ProjectId = 1,
                State = 2
            };

            tasksService.UpdateTask(task);

            A.CallTo(() => _fakeUnitOfWork.Tasks.Update(A<Task>._)).MustHaveHappenedOnceExactly();
        }


        [Fact]
        public void AddUserToTeam_WhenTrueDTO_ThenAdded()
        {
            var user = _usersService.GetUsers(true).FirstOrDefault(u => u.TeamId == null);

            int indexUser = user.Id;

            user.TeamId = 2;

            _usersService.UpdateUser(user);

            var userChanged = _usersService.FindUserById(indexUser);

            Assert.Equal(2, userChanged.TeamId);

        }

        [Fact]
        public void AddUserToTeam_WhenCallServiceMethod_ThenCallDalMethod()
        {
            UsersService usersService = new UsersService(_fakeUnitOfWork, _imapper);

            var userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                TeamId = 2,
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            };

            usersService.UpdateUser(userDTO);

            A.CallTo(() => _fakeUnitOfWork.Users.Update(A<User>._)).MustHaveHappenedOnceExactly();

        }


        [Theory]
        [InlineData(1, new int[] { 117, 177 })]
        [InlineData(2, new int[] { 2, 37, 76, 181 })]
        [InlineData(3, new int[] { 57, 64, 86, 115, 130, 138, 167, 194 })]
        [InlineData(4, new int[] { 87, 129, 171, 173 })]
        [InlineData(5, new int[] { 155 })]

        public void GetUnfinishedTasks_WhenTrueId_ThenGetArray(int userId, int[] expectedArray)
        {
            List<TaskDTO> result = _tasksService.GetUnfinishedTasks(userId).ToList();

            int[] actualArray = result.Select(t => t.Id).ToArray();

            Array.Sort(actualArray);
            Array.Sort(expectedArray);

            // Assert

            Assert.True(actualArray.SequenceEqual(expectedArray));
        }


        [Theory]
        [InlineData(9999)]
        [InlineData(-1)]
        [InlineData(0)]

        public void GetUnfinishedTasks_WhenFalseId_ThenThrowArgumentException(int userId)
        {
            Assert.Throws<ArgumentException>(() => _tasksService.GetUnfinishedTasks(userId));
        }


    }
}
