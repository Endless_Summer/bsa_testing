﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Entities
{
    public class Team : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public virtual List<User> Users { get; set; }
        public virtual List<Project> Projects { get; set; }

    }

}
