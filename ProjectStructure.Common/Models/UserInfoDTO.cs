﻿namespace ProjectStructure.Common.Models
{
    public class UserInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int TasksLastProject { get; set; }
        public int NotComletedTasks { get; set; }
        public TaskDTO MaxTask { get; set; }
    }
}
