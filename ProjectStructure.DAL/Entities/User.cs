﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public virtual Team Team { get; set; }
        public virtual List<Project> Projects { get; set; }
        public virtual List<Task> Tasks { get; set; }
    }
}
