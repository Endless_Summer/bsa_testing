﻿using Microsoft.Extensions.Caching.Distributed;
using ProjectStructure.DAL.Entities;
using System;

namespace ProjectStructure.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<User> Users { get; }
        IGenericRepository<Team> Teams { get; }
        IGenericRepository<TaskState> TaskStates { get; }
        IGenericRepository<Task> Tasks { get; }
        IGenericRepository<Project> Projects { get; }

        void Save();

    }
}
