﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITasksService
    {
        public IEnumerable<TaskDTO> GetTasks(bool asNoTracking = false);
        TaskDTO FindTaskById(int id);
        void CreateTask(TaskDTO item);
        void RemoveTask(int id);
        void UpdateTask(TaskDTO item);
        IEnumerable<TaskDTO> GetUnfinishedTasks(int userId);

    }
}
