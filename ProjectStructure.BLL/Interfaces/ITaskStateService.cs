﻿using ProjectStructure.Common.Models;
using System.Collections.Generic;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITaskStateService
    {
        IEnumerable<TaskStateDTO> GetTaskStates();
        TaskStateDTO FindTaskStateById(int id);
        void CreateTaskState(TaskStateDTO item);
        void RemoveTaskState(int id);
        void UpdateTaskState(TaskStateDTO item);
    }
}
