﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.Common.Models;
using ProjectStructure.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class ControllersIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        ProjectStructureDbContext db;
        public ControllersIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    var descriptor = services.SingleOrDefault(
                        d => d.ServiceType ==
                            typeof(DbContextOptions<ProjectStructureDbContext>));

                    if (descriptor != null)
                    {
                        services.Remove(descriptor);
                    }

                    services.AddDbContext<ProjectStructureDbContext>(options => options.UseInMemoryDatabase("TestDB")); // , new InMemoryDatabaseRoot()
                    // Build the service provider.
                    var sp = services.BuildServiceProvider();
                    // Create a scope to obtain a reference to the database
                    using (var scope = sp.CreateScope())
                    {
                        var scopedServices = scope.ServiceProvider;
                        db = scopedServices.GetRequiredService<ProjectStructureDbContext>();

                        // Ensure the database is created.
                        db.Database.EnsureDeleted();
                        db.Database.EnsureCreated();
                    }
                });
            }
            ).CreateClient();

        }
        public void Dispose()
        {
            //_client.Dispose();
        }


        [Fact]
        public void AddProject_WhenTrueIdDTO_ThenAddedAndCodeOK()
        {
            // Кол-во записей перед добавлением
            var httpResponse = _client.GetAsync(@$"api/Projects").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int counProjectBeforeAdd = entity.Count;

            // Add
            ProjectDTO projectDTO = new ProjectDTO()
            {
                Name = "TestProject",
                Description = "Description",
                AuthorId = 1,
                TeamId = 2,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(6)
            };

            string jsonInString = JsonConvert.SerializeObject(projectDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = _client.PostAsync(@"api/Projects", stringContent).Result;

            // Кол-во записей после добавления
            httpResponse = _client.GetAsync(@$"api/Projects").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int counProjectAfterAdd = entity.Count;

            // Поиск добавленной записи
            var project = entity.FirstOrDefault(p => p.Name == "TestProject");


            Assert.Equal(HttpStatusCode.OK, httpResponsePost.StatusCode);
            Assert.True(counProjectAfterAdd - counProjectBeforeAdd == 1);
            Assert.NotNull(project);

        }

        [Fact]
        public void AddProject_WhenFalsedDTO_ThenCodeBadRequest()
        {
            // Кол-во записей перед добавлением
            var httpResponse = _client.GetAsync(@$"api/Projects").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);

            // Add
            ProjectDTO projectDTO = new ProjectDTO()
            {
                Name = "TestProject",
                Description = "Description",
                AuthorId = 9999,
                TeamId = 2,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(6)
            };

            string jsonInString = JsonConvert.SerializeObject(projectDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = _client.PostAsync(@"api/Projects", stringContent).Result;

            Assert.Equal(HttpStatusCode.BadRequest, httpResponsePost.StatusCode);
        }

        [Fact]
        public void DeleteUser_WhenTrueIdDTO_ThenDeletedAndCodeNoContent()
        {
            // Кол-во записей перед удалением
            var httpResponse = _client.GetAsync(@$"api/Users").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = _client.DeleteAsync(@"api/Users/4").Result;

            // Кол-во записей после удаления
            httpResponse = _client.GetAsync(@$"api/Users").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            // Поиск удаленной записи
            var user = entity.FirstOrDefault(u => u.Id == 4);

            Assert.Equal(HttpStatusCode.NoContent, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete - countProjectAfterDelete == 1);
            Assert.Null(user);

        }


        [Fact]
        public void DeleteUser_WhenFaulseIdDTO_ThenCodeBadRequest()
        {
            // Кол-во записей перед удалением
            var httpResponse = _client.GetAsync(@$"api/Users").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = _client.DeleteAsync(@"api/Users/9999").Result;

            // Кол-во записей после удаления
            httpResponse = _client.GetAsync(@$"api/Users").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<ProjectDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            Assert.Equal(HttpStatusCode.BadRequest, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete == countProjectAfterDelete);

        }

        [Fact]
        public void AddTeam_WhenTrueIdDTO_ThenAddedAndCodeOK()
        {
            // Кол-во записей перед добавлением
            var httpResponse = _client.GetAsync(@$"api/Teams").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<TeamDTO>>(stringResponse);
            int countTeamsBeforeAdd = entity.Count;

            // Add
            TeamDTO teamDTO = new TeamDTO()
            {
                Name = "TestTeam",
                CreatedAt = DateTime.Now
            };

            string jsonInString = JsonConvert.SerializeObject(teamDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = _client.PostAsync(@"api/Teams", stringContent).Result;

            // Кол-во записей после добавления
            httpResponse = _client.GetAsync(@$"api/Teams").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<TeamDTO>>(stringResponse);
            int countTeamsAfterAdd = entity.Count;

            // Поиск добавленной записи
            var team = entity.FirstOrDefault(p => p.Name == "TestTeam");


            Assert.Equal(HttpStatusCode.OK, httpResponsePost.StatusCode);
            Assert.True(countTeamsAfterAdd - countTeamsBeforeAdd == 1);
            Assert.NotNull(team);

        }


        [Fact]
        public void DeleteTask_WhenTrueIdDTO_ThenDeletedAndCodeNoContent()
        {
            // Кол-во записей перед удалением
            var httpResponse = _client.GetAsync(@$"api/Tasks").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = _client.DeleteAsync(@"api/Tasks/2").Result;

            // Кол-во записей после удаления
            httpResponse = _client.GetAsync(@$"api/Tasks").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            // Поиск удаленной записи
            var task = entity.FirstOrDefault(u => u.Id == 2);

            Assert.Equal(HttpStatusCode.NoContent, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete - countProjectAfterDelete == 1);
            Assert.Null(task);

        }


        [Fact]
        public void DeleteTask_WhenFaulseIdDTO_ThenCodeBadRequest()
        {
            // Кол-во записей перед удалением
            var httpResponse = _client.GetAsync(@$"api/Tasks").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectBeforeDelete = entity.Count;

            var httpResponseDelete = _client.DeleteAsync(@"api/Tasks/9999").Result;

            // Кол-во записей после удаления
            httpResponse = _client.GetAsync(@$"api/Tasks").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);
            int countProjectAfterDelete = entity.Count;

            Assert.Equal(HttpStatusCode.BadRequest, httpResponseDelete.StatusCode);
            Assert.True(countProjectBeforeDelete == countProjectAfterDelete);

        }

        [Fact]
        public void AddUser_WhenTrueIdDTO_ThenAddedAndCodeOK()
        {
            // Кол-во записей перед добавлением
            var httpResponse = _client.GetAsync(@$"api/Users").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<UserDTO>>(stringResponse);
            int countUsersBeforeAdd = entity.Count;

            // Add
            UserDTO userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                Birthday = DateTime.Now.AddYears(-20),
                RegisteredAt = DateTime.Now,
                TeamId = 2,
            };

            string jsonInString = JsonConvert.SerializeObject(userDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = _client.PostAsync(@"api/Users", stringContent).Result;

            // Кол-во записей после добавления
            httpResponse = _client.GetAsync(@$"api/Users").Result;
            stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            entity = JsonConvert.DeserializeObject<List<UserDTO>>(stringResponse);
            int countUsersAfterAdd = entity.Count;

            // Поиск добавленной записи
            var project = entity.FirstOrDefault(p => p.FirstName == "FirstName");


            Assert.Equal(HttpStatusCode.OK, httpResponsePost.StatusCode);
            Assert.True(countUsersAfterAdd - countUsersBeforeAdd == 1);
            Assert.NotNull(project);

        }

        [Fact]
        public void AddUser_WhenFalsedDTO_ThenCodeBadRequest()
        {
            // Add
            UserDTO userDTO = new UserDTO()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "email@mail.com",
                Birthday = DateTime.Now.AddYears(-20),
                RegisteredAt = DateTime.Now,
                TeamId = 9999,
            };

            string jsonInString = JsonConvert.SerializeObject(userDTO);
            StringContent stringContent = new StringContent(jsonInString, Encoding.UTF8, "application/json");
            var httpResponsePost = _client.PostAsync(@"api/Users", stringContent).Result;

            Assert.Equal(HttpStatusCode.NotFound, httpResponsePost.StatusCode);
        }


        [Theory]
        [InlineData(1, new int[] { 117, 177 })]
        [InlineData(2, new int[] { 2, 37, 76, 181 })]
        [InlineData(3, new int[] { 57, 64, 86, 115, 130, 138, 167, 194 })]
        [InlineData(4, new int[] { 87, 129, 171, 173 })]
        [InlineData(5, new int[] { 155 })]

        public void GetUnfinishedTasks_WhenTrueId_ThenGetArray(int userId, int[] expectedArray)
        {
            // Кол-во записей перед удалением
            var httpResponse = _client.GetAsync(@$"api/Tasks/UnfinishedTasksForUser/{userId}").Result;
            var stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
            var entity = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);

            int[] actualArray = entity.Select(t => t.Id).ToArray();

            Array.Sort(actualArray);
            Array.Sort(expectedArray);

            // Assert

            Assert.True(actualArray.SequenceEqual(expectedArray));
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);

        }



        [Theory]
        [InlineData(9999)]
        [InlineData(-1)]
        [InlineData(0)]

        public void GetUnfinishedTasks_WhenFaulseId_ThenThrowArgumentException(int userId)
        {
            var httpResponse = _client.GetAsync(@$"api/Tasks/UnfinishedTasksForUser/{userId}").Result;

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);

        }

    }
}
